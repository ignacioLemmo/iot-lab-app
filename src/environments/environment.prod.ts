export const environment = {
  production: true,
  redirectUriAuth0:'http://app-iotlab.azurewebsites.net/callback',
  apiUrl:"http://api-aiotlab.azurewebsites.net/api",
};
