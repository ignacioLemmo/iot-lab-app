import { MdDialog } from '@angular/material';
import { HubGuardService } from './services/hub-guard.service';
import { LocalHubService } from './services/local-hub.service';
//modulos
import { Http } from '@angular/http';
import { HttpModule, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ToastyModule } from 'ng2-toasty';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from './material/material.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


//router
import { APP_ROUTING } from './app.router';

//servicios
import { TestTokenService } from './services/test-token.service';
import { AuthService } from './services/auth.service';
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { AppGuardService } from "app/services/app-guard.service";

//componentes
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProfileComponent } from './components/profile/profile.component';
import { DevicesComponent } from './components/devices/devices.component';
import { MainComponent } from './components/main/main.component';
import { DeviceManagerComponent } from './components/devices/device-manager.component';
import { HubDialogComponent } from './components/material-components/hub-dialog.component';
import { HubComponent } from './components/hub/hub.component';

export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(new AuthConfig({
    tokenGetter: (() => localStorage.getItem('access_token'))
  }), http, options);
}

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    DashboardComponent,
    ProfileComponent,
    DevicesComponent,
    MainComponent,
    DeviceManagerComponent,
    HubDialogComponent,
    HubComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    HttpModule,
    ToastyModule.forRoot(),
    MaterialModule,
    BrowserAnimationsModule,
    FormsModule
  ],
  providers: [
    TestTokenService,
    AuthService,
    AppGuardService,
    LocalHubService,
    HubGuardService,
    {
      provide: AuthHttp,
      useFactory: authHttpServiceFactory,
      deps: [Http, RequestOptions]
    }],
  bootstrap: [AppComponent],
  entryComponents: [HubDialogComponent]
})
export class AppModule { }
