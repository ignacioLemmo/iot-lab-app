import { HubComponent } from './components/hub/hub.component';
import { DeviceManagerComponent } from './components/devices/device-manager.component';
import { MainComponent } from './components/main/main.component';
import { HubGuardService } from './services/hub-guard.service';
import { DevicesComponent } from './components/devices/devices.component';
import { ProfileComponent } from './components/profile/profile.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AppGuardService } from './services/app-guard.service';
import {Routes,RouterModule} from '@angular/router';
import { HomeComponent } from './components/home/home.component';

const APP_ROUTES: Routes = [
    { path: 'home', component: HomeComponent},
    { 
        path: 'dashboard', 
        component: DashboardComponent, 
        canActivate:[AppGuardService],
        children:[
            { path: 'main', component: MainComponent },
            { path: 'devices', component: DevicesComponent},
            { path: 'devices/new', component: DeviceManagerComponent , canActivate:[HubGuardService] },
            { path: 'devices/edit/:id', component: DeviceManagerComponent , canActivate:[HubGuardService] },
            { path: 'devices/delete/:id', component: DeviceManagerComponent , canActivate:[HubGuardService] },
            { path: 'hubs', component: HubComponent},
            { path: 'profile', component: ProfileComponent },
            { path: '**', pathMatch: 'full',redirectTo:'main' }
        ]
    },
    
    { path: '**', pathMatch: 'full',redirectTo:'home' }
];

export const APP_ROUTING= RouterModule.forRoot(APP_ROUTES)