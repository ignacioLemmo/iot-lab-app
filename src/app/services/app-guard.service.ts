import { Observable } from 'rxjs/Observable';
import { environment } from 'environments/environment';
import { AuthHttp } from 'angular2-jwt/angular2-jwt';
import { Http } from '@angular/http';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { CanActivate } from "@angular/router/src";

@Injectable()
export class AppGuardService implements CanActivate {

  userProfile:any={};

  constructor(private auth:AuthService, private http:Http, private authHttp:AuthHttp) {
    
   }

  canActivate():boolean{
        
        if(this.auth.isAuthenticated()){
              return true;
        }
        else{
          this.auth.login();
          return false;
        }
    
      }
}
