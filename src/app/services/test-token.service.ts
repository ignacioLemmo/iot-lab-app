import { environment } from 'environments/environment';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';
import { AuthHttp } from "angular2-jwt";
import { Http } from "@angular/http";
@Injectable()
export class TestTokenService {

  constructor(private _http:Http, private authHttp: AuthHttp) { }

    getValues(){
     return this.authHttp.get(environment.apiUrl+"/values")
        .map(res=>res.json());
    }
}
