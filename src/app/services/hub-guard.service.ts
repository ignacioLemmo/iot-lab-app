import { AuthService } from 'app/services/auth.service';
import { environment } from 'environments/environment';
import { AuthHttp } from 'angular2-jwt';
import { Http } from '@angular/http';
import { LocalHubService } from 'app/services/local-hub.service';
import {Router} from '@angular/router';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router/src';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { AppGuardService } from "app/services/app-guard.service";

@Injectable()
export class HubGuardService implements CanActivate {

  userProfile: any = {};

  constructor(private auth: AuthService, private http: Http, privateauthHttp: AuthHttp, private router: Router) {

  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {

    this.userProfile = JSON.parse(localStorage.getItem('auth0Profile'));

    if (this.auth.isAuthenticated()) {

      return this.http.get(environment.apiUrl + "/localhubcontroller/getbyemail/" + this.userProfile.email).map((data) => {
        if (data.json().userHaveHub) {
          console.log('tiene hub');
          return true;
        }
        console.log('no tiene hub');

        this.router.navigate(['/dashboard/hubs'],{ queryParams: { hub: 'no' } });

        return false;
      })

    }
    else {
      this.auth.login();
      return false;
    }

  }

}
