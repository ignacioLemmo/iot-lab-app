import { environment } from 'environments/environment';
import { AuthHttp } from 'angular2-jwt';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable()
export class LocalHubService {

  constructor(private http:Http, private authHttp:AuthHttp) { }

  checkHub(email:string){

     return this.http.get(environment.apiUrl+"/localhubcontroller/getbyemail/"+email)
        .map(res=>res.json());
  }
}
