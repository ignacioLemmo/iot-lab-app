import { MdDialog } from '@angular/material';
import { ActivatedRoute, Params } from '@angular/router';
import { LocalHubService } from './../../services/local-hub.service';
import { Component, OnInit } from '@angular/core';
import { HubDialogComponent } from "app/components/material-components/hub-dialog.component";

@Component({
  selector: 'app-hub',
  templateUrl: './hub.component.html',
  styles: []
})
export class HubComponent implements OnInit {

  constructor(private localHubService:LocalHubService, 
    private activeRoute:ActivatedRoute,
    public dialog: MdDialog) 
    {

    }

      ngOnInit() {
        this.activeRoute.queryParams.subscribe( (params:Params) => { 
          
          if(params.hub){
            console.log(params.hub);
            let dialog = this.dialog.open(HubDialogComponent,{
              width: '250px',
              data: { titulo:"Crear Hub",msj:"Se necesita crear un hub para poder agregar dispositivos" }
            });
          }
       })
      }

}
