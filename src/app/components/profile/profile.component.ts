import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styles: []
})
export class ProfileComponent implements OnInit {

  profile:any={};

  constructor() {}

  ngOnInit() {

    this.profile = JSON.parse(localStorage.getItem('auth0Profile'));

  }

}
