import { Router } from '@angular/router';
import { Component, OnInit, Inject } from '@angular/core';
import { MdDialogRef, MD_DIALOG_DATA } from "@angular/material";

@Component({
  selector: 'app-hub-dialog',
  templateUrl: './hub-dialog.component.html',
  styles: []
})
export class HubDialogComponent {

  constructor(public dialogRef: MdDialogRef<HubDialogComponent>, 
    @Inject(MD_DIALOG_DATA) public data: any, 
    private router: Router) { }
  
    confirmSelection() {
      this.dialogRef.close();
    }

    goToHub(){
      this.dialogRef.close();
      this.router.navigate(['/dashboard/hubs']);
    }


}
