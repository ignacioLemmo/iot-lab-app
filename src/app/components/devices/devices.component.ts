import { LocalHubService } from 'app/services/local-hub.service';
import { TestTokenService } from './../../services/test-token.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MdDialog } from '@angular/material';
import { HubDialogComponent } from "app/components/material-components/hub-dialog.component";

@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styles: []
})
export class DevicesComponent implements OnInit {

  userProfile: any = {};

  constructor(private localHubService:LocalHubService, 
    private activeRoute:ActivatedRoute,
    public dialog: MdDialog,
    private router: Router) 
    {
      this.userProfile = JSON.parse(localStorage.getItem('auth0Profile'));
    }

      ngOnInit() {
      }

      goDevecieManager(){

        this.localHubService.checkHub(this.userProfile.email).subscribe(data=>{
         if(!data.userHaveHub){
          let dialog = this.dialog.open(HubDialogComponent,{
            width: '250px',
            data: { titulo:"Crear Hub",msj:"Se necesita crear un hub para poder agregar dispositivos" }
          });
         }
        });

      }
}
