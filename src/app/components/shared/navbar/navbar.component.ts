import { AuthService } from './../../../services/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: []
})
export class NavbarComponent implements OnInit {
  auth0Profile:any={};
  constructor(public auth:AuthService) {}

 ngOnInit() {
  if (this.auth.userProfile) {
    this.auth0Profile = JSON.parse(localStorage.getItem('auth0Profile'));
    console.log("Error al obtener profile de usuario");

  } else {
    this.auth.getProfile((err, profile) => {
      localStorage.setItem('auth0Profile',JSON.stringify( this.auth.userProfile));
      this.auth0Profile = JSON.parse(localStorage.getItem('auth0Profile'));
      console.log(this.auth0Profile);
    });
  }
 }
 login(){
   this.auth.login();
 }
 logout(){
   this.auth.logout();
 }
}