import { IOTLabWebAppPage } from './app.po';

describe('iotlab-web-app App', () => {
  let page: IOTLabWebAppPage;

  beforeEach(() => {
    page = new IOTLabWebAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
